const { createProject, getProjects, getProjectById, updateProject, deleteProject } = require('@services/projectService');

async function createProjectController(req, res) {
  try {
    const project = await createProject(req.body);
    res.status(201).json(project);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}

async function getProjectsController(req, res) {
  try {
    const projects = await getProjects();
    res.status(200).json(projects);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}

async function getProjectByIdController(req, res) {
  try {
    const project = await getProjectById(req.params.id);
    if (project) {
      res.status(200).json(project);
    } else {
      res.status(404).json({ message: 'Project not found' });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}

async function updateProjectController(req, res) {
  try {
    const project = await updateProject(req.params.id, req.body);
    res.status(200).json(project);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}

async function deleteProjectController(req, res) {
  try {
    await deleteProject(req.params.id);
    res.status(204).send();
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}

module.exports = { createProjectController, getProjectsController, getProjectByIdController, updateProjectController, deleteProjectController };