const { createProjectTask, getProjectTasks, getProjectTaskById, updateProjectTask, deleteProjectTask } = require('@services/projectTaskService');

async function createProjectTaskController(req, res) {
  try {
    const projectTask = await createProjectTask(req.body);
    res.status(201).json(projectTask);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}

async function getProjectTasksController(req, res) {
  try {
    const projectTasks = await getProjectTasks();
    res.status(200).json(projectTasks);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}

async function getProjectTaskByIdController(req, res) {
  try {
    const projectTask = await getProjectTaskById(req.params.id);
    if (projectTask) {
      res.status(200).json(projectTask);
    } else {
      res.status(404).json({ message: 'Project Task not found' });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}

async function updateProjectTaskController(req, res) {
  try {
    const updatedProjectTask = await updateProjectTask(req.params.id, req.body);
    res.status(200).json(updatedProjectTask);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}

async function deleteProjectTaskController(req, res) {
  try {
    await deleteProjectTask(req.params.id);
    res.status(204).send();
  } catch (error) {
    if (error.message.includes('ProjectTask could not be found')) {
      res.status(404).json({ message: 'Project Task not found' });
    } else {
      res.status(500).json({ message: error.message });
    }
  }
}

module.exports = { createProjectTaskController, getProjectTasksController, getProjectTaskByIdController, updateProjectTaskController, deleteProjectTaskController };