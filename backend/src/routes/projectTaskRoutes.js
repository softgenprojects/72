const express = require('express');
const router = express.Router();
const { createProjectTaskController, getProjectTasksController, getProjectTaskByIdController, updateProjectTaskController, deleteProjectTaskController } = require('@controllers/projectTaskController');

router.post('/', createProjectTaskController);
router.get('/', getProjectTasksController);
router.get('/:id', getProjectTaskByIdController);
router.put('/:id', updateProjectTaskController);
router.delete('/:id', deleteProjectTaskController);

module.exports = router;