const express = require('express');
const router = express.Router();
const { createProjectController, getProjectsController, getProjectByIdController, updateProjectController, deleteProjectController } = require('@controllers/projectController');

router.post('/', createProjectController);
router.get('/', getProjectsController);
router.get('/:id', getProjectByIdController);
router.put('/:id', updateProjectController);
router.delete('/:id', deleteProjectController);

module.exports = router;