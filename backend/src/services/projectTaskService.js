const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

async function createProjectTask(projectTaskData) {
  return await prisma.projectTask.create({
    data: projectTaskData,
  });
}

async function getProjectTasks() {
  return await prisma.projectTask.findMany();
}

async function getProjectTaskById(id) {
  return await prisma.projectTask.findUnique({
    where: { id },
  });
}

async function updateProjectTask(id, projectTaskData) {
  return await prisma.projectTask.update({
    where: { id: parseInt(id, 10) },
    data: projectTaskData,
  });
}

async function deleteProjectTask(id) {
  const projectTask = await prisma.projectTask.findUnique({
    where: { id: parseInt(id, 10) },
  });
  if (!projectTask) {
    throw new Error('ProjectTask could not be found');
  }
  return await prisma.projectTask.delete({
    where: { id: parseInt(id, 10) },
  });
}

module.exports = { createProjectTask, getProjectTasks, getProjectTaskById, updateProjectTask, deleteProjectTask };