const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

async function createProject(data) {
  return await prisma.project.create({
    data,
  });
}

async function getProjects() {
  return await prisma.project.findMany();
}

async function getProjectById(id) {
  return await prisma.project.findUnique({
    where: { id: parseInt(id, 10) },
  });
}

async function updateProject(id, data) {
  return await prisma.project.update({
    where: { id: parseInt(id, 10) },
    data,
  });
}

async function deleteProject(id) {
  return await prisma.project.delete({
    where: { id: parseInt(id, 10) }
  });
}

module.exports = { createProject, getProjects, getProjectById, updateProject, deleteProject };