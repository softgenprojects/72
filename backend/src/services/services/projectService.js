const prisma = require('@prisma/client').prisma;

async function createProject(data) {
  try {
    const project = await prisma.project.create({
      data,
    });
    return project;
  } catch (error) {
    throw error;
  }
}

async function getProjects() {
  try {
    const projects = await prisma.project.findMany();
    return projects;
  } catch (error) {
    throw error;
  }
}

async function getProjectById(id) {
  try {
    const project = await prisma.project.findUnique({
      where: { id },
    });
    return project;
  } catch (error) {
    throw error;
  }
}

async function updateProject(id, data) {
  try {
    const project = await prisma.project.update({
      where: { id },
      data,
    });
    return project;
  } catch (error) {
    throw error;
  }
}

async function deleteProject(id) {
  try {
    const project = await prisma.project.delete({
      where: { id },
    });
    return project;
  } catch (error) {
    throw error;
  }
}

module.exports = {
  createProject,
  getProjects,
  getProjectById,
  updateProject,
  deleteProject,
};