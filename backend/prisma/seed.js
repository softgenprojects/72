const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

async function main() {
  const project1 = await prisma.project.create({
    data: {
      name: 'Project Alpha',
      description: 'This is the first project.',
      tasks: {
        create: [
          {
            name: 'Task 1',
            description: 'Task 1 description',
            status: 'In Progress'
          },
          {
            name: 'Task 2',
            description: 'Task 2 description',
            status: 'Completed'
          }
        ]
      }
    }
  });

  const project2 = await prisma.project.create({
    data: {
      name: 'Project Beta',
      description: 'This is the second project.',
      tasks: {
        create: [
          {
            name: 'Task 3',
            description: 'Task 3 description',
            status: 'Pending'
          },
          {
            name: 'Task 4',
            description: 'Task 4 description',
            status: 'In Progress'
          }
        ]
      }
    }
  });

  console.log({ project1, project2 });
}

main()
  .catch((e) => {
    throw e;
  })
  .finally(async () => {
    await prisma.$disconnect();
  });