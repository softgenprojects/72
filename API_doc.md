# Backend API Documentation

This document provides a comprehensive guide to the backend API endpoints available for frontend integration. It includes details on request methods, parameters, payload specifications, response formats, and expected status codes.

## Base URL

All API requests are made to the base URL: `https://72-api.app.softgen.ai`

## Endpoints

### Projects

- **Create a Project**
  - **POST** `/api/projects`
  - **Body**: `{ "name": "string", "description": "string" }`
  - **Response**: `201 Created`

- **Get All Projects**
  - **GET** `/api/projects`
  - **Response**: `200 OK`

- **Get Project by ID**
  - **GET** `/api/projects/:id`
  - **Response**: `200 OK` | `404 Not Found`

- **Update Project**
  - **PUT** `/api/projects/:id`
  - **Body**: `{ "name": "string", "description": "string" }`
  - **Response**: `200 OK`

- **Delete Project**
  - **DELETE** `/api/projects/:id`
  - **Response**: `204 No Content`

### Project Tasks

- **Create a Project Task**
  - **POST** `/api/projectTasks`
  - **Body**: `{ "projectId": "int", "name": "string", "description": "string", "status": "string" }`
  - **Response**: `201 Created`

- **Get All Project Tasks**
  - **GET** `/api/projectTasks`
  - **Response**: `200 OK`

- **Get Project Task by ID**
  - **GET** `/api/projectTasks/:id`
  - **Response**: `200 OK` | `404 Not Found`

- **Update Project Task**
  - **PUT** `/api/projectTasks/:id`
  - **Body**: `{ "name": "string", "description": "string", "status": "string" }`
  - **Response**: `200 OK`

- **Delete Project Task**
  - **DELETE** `/api/projectTasks/:id`
  - **Response**: `204 No Content`

## Authentication and Authorization

No authentication or authorization is required for accessing the API endpoints documented here.

## Error Handling

Responses will include an appropriate HTTP status code. Common codes include:

- `200 OK` - The request was successful.
- `201 Created` - A new resource was successfully created.
- `204 No Content` - The request was successful, but there is no content to send for this request.
- `404 Not Found` - The requested resource was not found.
- `500 Internal Server Error` - An error occurred on the server side.

## Examples

**Create a Project**
```
curl -X POST https://72-api.app.softgen.ai/api/projects -H 'Content-Type: application/json' -d '{"name":"Project Alpha","description":"This is the first project."}'
```

**Get All Projects**
```
curl -X GET https://72-api.app.softgen.ai/api/projects
```

**Update Project Task**
```
curl -X PUT https://72-api.app.softgen.ai/api/projectTasks/1 -H 'Content-Type: application/json' -d '{"name":"Updated Task Name","description":"Updated description","status":"Completed"}'
```