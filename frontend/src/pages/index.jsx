import { Box, VStack, Heading, Button, Text, useColorModeValue, Center, Tab, TabList, TabPanel, TabPanels, Tabs, Input, useDisclosure, SimpleGrid } from '@chakra-ui/react';
import { TopBar } from '@components/TopBar';
import { ProjectTypeSelector } from '@components/ProjectTypeSelector';
import { ProjectCard } from '@components/ProjectCard';
import { BrandLogo } from '@components/BrandLogo';
import { CreateProjectModal } from '@components/CreateProjectModal';
import { useGetAllProjects } from '@hooks/useProjects';
import { useGetAllProjectTasks } from '@hooks/useProjectTasks';

const Home = () => {
  const bgColor = useColorModeValue('gray.800', 'gray.800');
  const textColor = useColorModeValue('whiteAlpha.900', 'whiteAlpha.900');
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { data: projects, isLoading: projectsLoading, isError: projectsError } = useGetAllProjects();
  const { data: tasks, isLoading: tasksLoading, isError: tasksError } = useGetAllProjectTasks();

  if (projectsLoading || tasksLoading) return <Text>Loading...</Text>;
  if (projectsError || tasksError) return <Text>Error loading data</Text>;

  return (
    <Box bg={bgColor} color={textColor} minH="100vh">
      <TopBar />
      <Center flexDirection="column" py="12" px={{ base: '4', lg: '8' }}>
        <BrandLogo />
        <Heading as="h1" size="xl" textAlign="center" my="4">
          What do you want to build?
        </Heading>
        <Input placeholder="e.g. A todo app" variant="filled" mb="8" />
        <ProjectTypeSelector />
        <Button colorScheme="red" size="lg" my="8" onClick={onOpen}>
          Create
        </Button>
        <CreateProjectModal isOpen={isOpen} onClose={onClose} />
        <Tabs isFitted variant="enclosed" mb="8">
          <TabList mb='1em'>
            <Tab>Featured</Tab>
            <Tab>Latest</Tab>
            <Tab>My Projects</Tab>
          </TabList>
          <TabPanels>
            <TabPanel>
              <p>Featured Projects</p>
            </TabPanel>
            <TabPanel>
              <p>Latest Projects</p>
            </TabPanel>
            <TabPanel>
              <p>My Projects</p>
            </TabPanel>
          </TabPanels>
        </Tabs>
        <SimpleGrid columns={{ base: 1, md: 3 }} spacing="8">
          {projects.map(project => (
            <ProjectCard key={project.id} projectId={project.id} />
          ))}
        </SimpleGrid>
      </Center>
    </Box>
  );
};

export default Home;