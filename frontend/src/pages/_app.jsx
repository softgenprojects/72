import { ChakraProvider } from '@chakra-ui/react';
import { extendTheme, theme as baseTheme } from '@chakra-ui/react';
import { extendThemeWithDarkMode } from '@utilities/themeUtils';
import { MadeWithSGBadge } from '@components/MadeWithSGBadge';
import { QueryClient, QueryClientProvider } from 'react-query';

const queryClient = new QueryClient();

const darkTheme = extendThemeWithDarkMode(baseTheme);

export const theme = extendTheme(
  {
    colors: { ...baseTheme.colors, brand: baseTheme.colors.blue },
  },
  darkTheme,
);

export default function App({ Component, pageProps }) {
  return (
    <QueryClientProvider client={queryClient}>
      <ChakraProvider theme={theme}>
        <Component {...pageProps} />
        <MadeWithSGBadge/>
      </ChakraProvider>
    </QueryClientProvider>
  );
}