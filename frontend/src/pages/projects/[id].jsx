import { Box, Heading, Text, VStack, SimpleGrid, Spinner, Alert, AlertIcon, AlertTitle, AlertDescription } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useGetProjectById } from '@hooks/useProjects';
import { useGetProjectTasksByProjectId } from '@hooks/useProjectTasks';

const ProjectDetailsPage = () => {
  const router = useRouter();
  const { id: projectId } = router.query;

  useEffect(() => {
    if (!projectId) {
      router.push('/');
    }
  }, [projectId]);

  const { data: project, isLoading: projectLoading } = useGetProjectById(projectId);
  const { data: tasks, isLoading: tasksLoading, isError, error } = useGetProjectTasksByProjectId(projectId);

  if (projectLoading || tasksLoading) return <Spinner color='blue.500' />

  if (isError) return (
    <Alert status='error'>
      <AlertIcon />
      <AlertTitle mr={2}>Failed to load tasks!</AlertTitle>
      <AlertDescription>{error.message}</AlertDescription>
    </Alert>
  );

  return (
    <Box p={5}>
      <VStack spacing={4} align='stretch'>
        <Heading>{project?.name}</Heading>
        <Text>{project?.description}</Text>
        <Heading size='md'>Tasks</Heading>
        <SimpleGrid columns={{ base: 1, md: 2, lg: 3 }} spacing={5}>
          {tasks?.map(task => (
            <Box key={task.id} p={5} shadow='md' borderWidth='1px'>
              <Heading size='sm'>{task.name}</Heading>
              <Text mt={4}>{task.description}</Text>
            </Box>
          ))}
        </SimpleGrid>
      </VStack>
    </Box>
  );
};

export default ProjectDetailsPage;