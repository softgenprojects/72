export const extendThemeWithDarkMode = (baseTheme) => {
  const extendedTheme = {
    ...baseTheme,
    colors: {
      ...baseTheme.colors,
      background: '#000',
      text: '#fff',
      primary: '#ff0000',
      secondary: '#808080',
      accent: '#4A90E2',
      hover: {
        background: '#333',
        text: '#fff'
      },
      active: {
        background: '#555',
        text: '#fff'
      },
      input: {
        background: 'transparent',
        text: '#fff',
        placeholder: '#ccc'
      },
      projectCard: {
        background: '#222',
        text: '#fff',
        meta: '#777'
      },
      tab: {
        active: '#fff',
        inactive: '#aaa'
      }
    }
  };
  return extendedTheme;
};