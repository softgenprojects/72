import axios from 'axios';

export const createProjectTask = async (taskData) => {
  try {
    const response = await axios.post('https://72-api.app.softgen.ai/api/projectTasks', taskData);
    return response.data;
  } catch (error) {
    console.error('Error creating project task:', error);
    throw error;
  }
};

export const getAllProjectTasks = async () => {
  try {
    const response = await axios.get('https://72-api.app.softgen.ai/api/projectTasks');
    return response.data;
  } catch (error) {
    console.error('Error fetching all project tasks:', error);
    throw error;
  }
};

export const getProjectTaskById = async (taskId) => {
  try {
    const response = await axios.get(`https://72-api.app.softgen.ai/api/projectTasks/${taskId}`);
    return response.data;
  } catch (error) {
    console.error('Error fetching project task by ID:', error);
    throw error;
  }
};

export const updateProjectTask = async (taskId, taskData) => {
  try {
    const response = await axios.put(`https://72-api.app.softgen.ai/api/projectTasks/${taskId}`, taskData);
    return response.data;
  } catch (error) {
    console.error('Error updating project task:', error);
    throw error;
  }
};

export const deleteProjectTask = async (taskId) => {
  try {
    const response = await axios.delete(`https://72-api.app.softgen.ai/api/projectTasks/${taskId}`);
    return response.data;
  } catch (error) {
    console.error('Error deleting project task:', error);
    throw error;
  }
};

export const getProjectTasksByProjectId = async (projectId) => {
  try {
    const response = await axios.get(`https://72-api.app.softgen.ai/api/projectTasks?projectId=${projectId}`);
    return response.data;
  } catch (error) {
    console.error('Error fetching project tasks by project ID:', error);
    throw error;
  }
};