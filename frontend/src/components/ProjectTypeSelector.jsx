import React from 'react';
import { HStack, Box, Text, useColorModeValue } from '@chakra-ui/react';

export const ProjectTypeSelector = () => {
  const projectTypes = ['Todo', 'Personal website', 'Notes', 'Band website', 'Chat', 'Dashboard'];
  const hoverBg = useColorModeValue('gray.200', 'gray.700');
  const bg = useColorModeValue('gray.100', 'gray.800');

  return (
    <HStack spacing={4} overflowX='auto' p={4} bg={bg}>
      {projectTypes.map((type) => (
        <Box
          key={type}
          p={2}
          borderRadius='md'
          bg={bg}
          _hover={{ bg: hoverBg }}
          cursor='pointer'
        >
          <Text fontSize='md'>{type}</Text>
        </Box>
      ))}
    </HStack>
  );
};