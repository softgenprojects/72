import React from 'react';
import { Box, Flex, Text, Menu, MenuButton, MenuList, MenuItem, Button, Avatar, useColorMode, useColorModeValue, IconButton } from '@chakra-ui/react';
import { ChevronDownIcon, MoonIcon, SunIcon } from '@chakra-ui/icons';

export const TopBar = () => {
  const { colorMode, toggleColorMode } = useColorMode();

  return (
    <Box bg={useColorModeValue('gray.100', 'gray.900')} w="100%" p={4} color={useColorModeValue('gray.800', 'white')}>
      <Flex justify="space-between" align="center">
        <Text fontSize="2xl">GPT Engineer</Text>
        <Flex align="center">
          <Menu>
            <MenuButton as={Button} rightIcon={<ChevronDownIcon />} variant="ghost" sx={{
              _hover: { bg: useColorModeValue('gray.200', 'gray.700') },
              borderRadius: 'md'
            }}>
              Projects
            </MenuButton>
            <MenuList bg={useColorModeValue('white', 'gray.700')}>
              <MenuItem _hover={{ bg: useColorModeValue('gray.100', 'gray.600') }}>Project 1</MenuItem>
              <MenuItem _hover={{ bg: useColorModeValue('gray.100', 'gray.600') }}>Project 2</MenuItem>
              <MenuItem _hover={{ bg: useColorModeValue('gray.100', 'gray.600') }}>Project 3</MenuItem>
            </MenuList>
          </Menu>
          <Box ml={4} display="flex" alignItems="center">
            <Avatar name="Marko Kraemer" src="https://i.pravatar.cc/300" size="sm" />
            <Text ml={2}>Marko Kraemer</Text>
          </Box>
          <IconButton
            aria-label="Toggle color mode"
            icon={colorMode === 'light' ? <MoonIcon /> : <SunIcon />}
            onClick={toggleColorMode}
            ml={4}
            variant="ghost"
          />
        </Flex>
      </Flex>
    </Box>
  );
};