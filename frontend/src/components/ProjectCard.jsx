import React from 'react';
import { Box, Image, Text, useColorModeValue, VStack, GridItem } from '@chakra-ui/react';
import { useGetProjectById } from '@hooks/useProjects';
import Link from 'next/link';

export const ProjectCard = ({ projectId }) => {
  const { data: project, isLoading, isError } = useGetProjectById(projectId);
  const cardBg = useColorModeValue('gray.100', 'gray.700');
  const hoverBg = useColorModeValue('gray.200', 'gray.600');

  if (isLoading) return <Text>Loading...</Text>;
  if (isError || !project) return <Text>Project could not be fetched</Text>;

  return (
    <GridItem colSpan={{ base: 12, md: 4 }}>
      <Link href={`/projects/${projectId}`} passHref>
        <Box
          maxW='100%'
          borderWidth='1px'
          borderRadius='lg'
          overflow='hidden'
          bg={cardBg}
          _hover={{ bg: hoverBg, transform: 'scale(1.05)', transition: 'all .3s ease-in-out' }}
          cursor='pointer'
          shadow='md'
        >
          <Image src={project.thumbnail} alt={`Thumbnail of ${project.name}`} width='100%' objectFit='cover' />
          <VStack p='6' align='start'>
            <Text fontWeight='bold' fontSize='xl'>{project.name}</Text>
            <Text fontSize='sm'>{`Created ${project.creationTime} by ${project.creatorUsername}`}</Text>
          </VStack>
        </Box>
      </Link>
    </GridItem>
  );
};