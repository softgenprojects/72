import { Modal, ModalOverlay, ModalContent, ModalHeader, ModalFooter, ModalBody, ModalCloseButton, Button, Input, Textarea } from '@chakra-ui/react';
import { useCreateProject } from '@hooks/useProjects';

export const CreateProjectModal = ({ isOpen, onClose }) => {
  const { mutate: createProject } = useCreateProject();

  const handleSubmit = async (event) => {
    event.preventDefault();
    const projectName = event.target.elements.projectName?.value;
    const projectDescription = event.target.elements.projectDescription?.value;
    const projectData = { name: projectName, description: projectDescription };
    createProject(projectData, {
      onSuccess: () => {
        onClose();
      },
      onError: (error) => {
        console.error(error);
      }
    });
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Create New Project</ModalHeader>
        <ModalCloseButton />
        <form onSubmit={handleSubmit}>
          <ModalBody>
            <Input id='projectName' placeholder='Project Name' my={4} />
            <Textarea id='projectDescription' placeholder='Project Description' my={4} />
          </ModalBody>
          <ModalFooter>
            <Button colorScheme='blue' mr={3} type='submit'>
              Create
            </Button>
            <Button variant='ghost' onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </form>
      </ModalContent>
    </Modal>
  );
};