import { useQuery, useMutation } from 'react-query';
import { createProjectTask, getAllProjectTasks, getProjectTaskById, updateProjectTask, deleteProjectTask, getProjectTasksByProjectId } from '@api/ProjectTasksApi';

export const useCreateProjectTask = () => {
  return useMutation(createProjectTask);
};

export const useGetAllProjectTasks = () => {
  return useQuery('projectTasks', getAllProjectTasks);
};

export const useGetProjectTaskById = (taskId) => {
  return useQuery(['projectTask', taskId], () => getProjectTaskById(taskId));
};

export const useUpdateProjectTask = () => {
  return useMutation(({ taskId, taskData }) => updateProjectTask(taskId, taskData));
};

export const useDeleteProjectTask = () => {
  return useMutation(deleteProjectTask);
};

export const useGetProjectTasksByProjectId = (projectId) => {
  return useQuery(['projectTasksByProjectId', projectId], () => projectId ? getProjectTasksByProjectId(projectId) : Promise.resolve([]));
};