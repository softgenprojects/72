import { useMutation } from 'react-query';
import { createProject } from '@api/ProjectsApi';

export const useCreateProject = () => {
  const mutation = useMutation(createProject);
  return mutation;
};