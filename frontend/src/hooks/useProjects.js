import { useQuery, useMutation } from 'react-query';
import { createProject, getAllProjects, getProjectById, updateProject, deleteProject } from '@api/ProjectsApi';

export const useCreateProject = () => {
  return useMutation(createProject);
};

export const useGetAllProjects = () => {
  return useQuery('projects', getAllProjects);
};

export const useGetProjectById = (projectId) => {
  return useQuery(['project', projectId], () => projectId ? getProjectById(projectId) : { isLoading: true });
};

export const useUpdateProject = () => {
  return useMutation(updateProject);
};

export const useDeleteProject = () => {
  return useMutation(deleteProject);
};